#!/usr/bin/env bash

GIT_CMD=$1
ROOT_DIR=$2
PARAMS=$3

RED='\033[0;31m'
YELLOW='\033[0;33m'
NO_COLLOR='\033[0m'

set_ifs () {
  IFS='
  '
}

reset_ifs () {
  IFS=''
}

filter_root_dir () {
  set_ifs
  echo `find ${ROOT_DIR} -name ".git" -type d | sed 's/\/\.git$//'`
  reset_ifs
}

display_git_cmd () {
  echo "Executed command: $@"
}

display_info () {
  echo "Execute git command on one or more git repos"
  echo "Usage: "
  echo "git-mass <cmd> <project-dir> <extra opts>"
}

git_cmd_log () {
  local git_cmd="git log --all --grep=$PARAMS --pretty=oneline"
  local arr=(`filter_root_dir`)

  for i in ${arr[*]}; do
    cd $i
    printf "Current directory: ${RED}${i}${NO_COLLOR}"
    echo
    echo -e "${YELLOW}\"`eval ${git_cmd}`\"${NO_COLLOR}"
    cd ../..
  done
}

git_search_branch () {
  local git_cmd="git branch | grep -i $PARAMS"
  local arr=(`filter_root_dir`)

  for i in ${arr[*]}; do
    cd $i
    printf "Current directory: ${RED}${i}${NO_COLLOR}"
    echo
    echo -e "${YELLOW}\"`eval ${git_cmd}`\"${NO_COLLOR}"
    cd ../..
  done
}

git_check_uncomitted () {
  local git_cmd="git status -sb"
  local arr=(`filter_root_dir`)

  for i in ${arr[*]}; do
    cd $i
    printf "Current directory: ${RED}${i}${NO_COLLOR}"
    echo
    echo -e "${YELLOW}\"`eval ${git_cmd}`\"${NO_COLLOR}"
    cd ../..
  done
}

determine_cmd () {
  case $1 in
    uncommited)
      git_check_uncomitted
      ;;
    branch)
      git_search_branch
      ;;
    log)
      git_cmd_log
      ;;
    *)
      display_info
      exit 1
  esac
}

determine_cmd $GIT_CMD
